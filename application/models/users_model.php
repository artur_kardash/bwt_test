
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users_model extends CI_Model 
{

	public function create ($user_data)
	{
       return $this->db->insert('users', $user_data);
	}

    public function is_registered($login_data)
    {    
       $result=$this->db->get_where('users', array('email'=>$login_data['email'], 'password'=>$login_data['password'])); 
       if($result->num_rows()>0)
       {
         return true;
       }else{
         return false;
       }
    }

    public function create_feed ($feed)
    {
        return $this->db->insert('feedback', $feed);
    }

    public function get_feed() 
    {
        $this->db->select('*');
        $this->db->from('feedback');
        $query = $this->db->get();
        if ($query)
        {
              return $query->result_array();
        }
              return false;
    }
}