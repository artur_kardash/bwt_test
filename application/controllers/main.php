<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller 
{

		function __construct()
		{
			parent::__construct();
			$this->load->model('users_model');
		}


		public function index()
		{
			$this->home();
		}

		public function home()
		{
			$data['title'] = "Главная";
			$this->load->view('head', $data);
			$this->load->view('home');
			$this->load->view('footer');
		}

		public function registr()
	  	{
		    $data['title'] = 'Регистрация';
		    $this->load->view('head', $data);
		    $this->load->view('home');
		    $this->load->view('registr');
		    $this->load->view('footer');
	  	}

	  	public function registr_db()
  		{
			 $this->load->library("form_validation");    
			 $this->form_validation->set_rules('users[email]', 'E-mail', 'trim|required|valid_email');
			 $this->form_validation->set_rules('users[name]', 'Имя', 'trim|required');
			 $this->form_validation->set_rules('users[last_name]', 'Фамилия', 'trim|required');
			 $this->form_validation->set_rules('users[password]', 'Пароль', 'trim|required|min_length[4]|md5');
	         if($this->form_validation->run())
	    	{
				 $user = $this->input->post('users');
				 $this->load->model('users_model');
				 if ($this->users_model->create($user))
				{
					  $this->session->set_userdata(array('is_logged_in'=>true));
					  $data['title'] = 'Регистрация прошла успешно';
					  $this->load->view('head', $data);
					  $this->load->view('home');
					  $this->load->view('footer');
					  } else {
					  $data['title'] = 'Нет доступа';
					  $this->load->view('head', $data);
					  $this->load->view('home');
					  $this->load->view('footer');              
					  }
	    	}
		}

		public function logout()
        {
			$this->session->sess_destroy();
			header("Location: /");
        }

    	 public function login_form()
    	{
            $data['title'] = 'Вход';
            $this->load->view('head', $data);
            $this->load->view('home');
            $this->load->view('login_form');
            $this->load->view('footer');
   		 }

	  	 public function login()
	     {   
	      $this->load->library('form_validation');
	      $this->form_validation->set_rules('users[email]', 'Email', 'required|trim|valid_email');
	      $this->form_validation->set_rules('users[password]', 'Password', 'required|md5|trim|min_length[4]');
		  if($this->form_validation->run())
	      {	  
	          $user=$this->input->post('users');
	          $this->load->model('users_model');
			  if($this->users_model->is_registered($user))
	          {
				  $this->session->set_userdata(array('is_logged_in'=>true));
				  header("Location: /");
				  die();
			  }else{}
	      }
		  $data['title'] = 'Неправельный логин либо пароль!';
	      $this->load->view('head', $data);
	      $this->load->view('home');
	      $this->load->view('login_form');
	      $this->load->view('error');
	      $this->load->view('footer');
		} 

		public function feedback_form()
		{
			$data['title'] = "Форма обратной связи";
			$this->load->view('head', $data);
			$this->load->view('home');
			$captcha = $this->captcha();
			$this->load->view('feedback_form', array('capt'=>$captcha));
			$this->load->view('footer');
		}

		public function feedback_db()
		{
			$this->load->library('form_validation');
		 	$this->form_validation->set_rules('feedback[email]', 'E-mail', 'trim|required|valid_email');
	        $this->form_validation->set_rules('feedback[name]', 'Имя', 'trim|required');
	        $this->form_validation->set_rules('feedback[message]', 'Сообщение', 'trim|required');
            $this->form_validation->set_rules('captcha', 'Captcha', 'required|trim|callback_captcha_validation');
	        if($this->form_validation->run())
	     	{	
				 $feed = $this->input->post('feedback');
				 $this->users_model->create_feed($feed);
				 $config=array(
					'protokol'=>'smtp',
					'smtp_host'=>'ssl://smtp.googlemail.com',
					'smtp_port'=>465,
					'smtp_user'=>'youmail@gmail.com',
					'smtp_pass'=>'*******'
					);

				$this->load->library('email', $config);
				$this->email->set_newline("\r\n");
				$this->email->from($feed['email']);
				$this->email->to('youmail@gmail.com'); 
				$this->email->subject('Тест Email');
				$this->email->message($feed['message']);	
				$this->email->send();
				$this->home();
				$this->load->view('success');
			}else{		 
				$data['title'] = 'Некорректно введены данные!';
				$this->load->view('head', $data);
				$this->load->view('home');
				$captcha = $this->captcha();
				$this->load->view('feedback_form', array('capt'=>$captcha));
				$this->load->view('error_form');
				$this->load->view('footer');               		
	     		}	               
		}

		public function captcha()
        {
			$this->load->helper('string');
			$this->load->helper('captcha');
			$string = random_string('numeric', 5);
			$vals = array(
				'word'  => $string,
				'img_path'  => 'image/captcha/',
				'img_url' => '/image/captcha/',
				'font_path' => '.sysem/fonts/texb.ttf',
				'img_width' => 150,
				'img_height' => 30,
				'expiration' => 60
			);           
			$cap = create_captcha($vals);
			$this->session->set_userdata('captcha', $cap['word']);
			return $cap['image'];
        }

		    
		public function captcha_validation()
		{
			$this->load->library('form_validation');
			if (strtolower($this->session->userdata('captcha')) != strtolower($this->input->post('captcha'))) 
			{
				$this->form_validation->set_message('captcha_validation', 'Wrong symbols of Captcha!');
				return false;
			}else{
				return true;
			}
		}

		public function message()
		{
			  $data['title'] = 'Сообщения';
			  $feed_data['feed'] = $this->users_model->get_feed();
			  $this->load->view('head', $data);
			  $this->load->view('home');
			  $this->load->view('message', $feed_data);
			  $this->load->view('footer');
		}

	  	 public function weather()
	  	{  	
			$data['title']='Погода в Запорожье';
	  		$this->load->view('head', $data);
	  		$content = file_get_contents('https://www.gismeteo.ua/weather-zaporizhzhya-5093/');
        	$price = preg_match_all('/<table>(.+?)<\/table>/si', $content, $all);
	  		$this->load->view('home');
	  		$this->load->view('weather', array('all'=>$all));
	  		$this->load->view('footer');
	  	}

}

