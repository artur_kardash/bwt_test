<div class="container">
  <div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-6">
      <h1 style="text-align: center;">Обратная форма</h1>
      <form class="form-horizontal" action="//formspree.io/Kardash1989@gmail.com" method="POST">
      <div class="form-group"><label class="col-sm-2 control-label" for="name">Имя</label>
      <div class="col-sm-10"><input id="name" class="form-control" name="" type="text" placeholder="Имя" /></div>
      </div>
      <div class="form-group"><label class="col-sm-2 control-label" for="email">Email</label>
      <div class="col-sm-10"><input id="email" class="form-control" name="_replyto" type="email" placeholder="Email" /> <input name="_next" type="hidden"  value="http://bwt_test.loc/" /> <input name="_subject" type="hidden" value="Обратная связь!" /> <input name="_cc" type="hidden" value="another@email.com" /> <input     style="display: none;" name="_gotcha" type="text" /></div>
      </div>
        <div class="form-group"><label class="col-sm-2 control-label" for="message">Сообщение</label>
      <div class="col-sm-10"><input id="message" class="form-control" type="text" placeholder="Сообщение" /></div>
      </div>
      <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10"><button class="btn btn-default" type="submit">Отправить!</button></div>
      </div>
      </form>
    </div>
    <div class="col-lg-3"></div>
  </div>
</div>