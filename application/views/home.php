<body>
<nav class="navbar navbar-inverse" role="navigation">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
      <a class="navbar-brand" href="/">Главная</a>
    </div>
    <?php if($this->session->userdata('is_logged_in')): ?>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li ><a href="/main/weather">Погода</a></li>
        <li><a href="/main/feedback_form">Обратная связь</a></li>
        <li><a href="/main/message">Список сообщений</a></li>
      </ul>
          <ul class="nav navbar-nav navbar-right">
            <li><a href="/main/logout">Выход</a></li>
          </ul>
    <?php else: ?>
           <ul class="nav navbar-nav navbar-right">
            <li><a href="/main/login_form">Вход</a></li>
            <li><a href="/main/registr">Регистрация</a></li>
          </ul>
     <?php endif; ?>     
    </div>
  </div>
</nav>