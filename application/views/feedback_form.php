<p><h2 style="color: #0d0f0d;padding:20px"> Обратная связь</h2></p>
<form class="form-horizontal" action = "/main/feedback_db" method = "post">
  <div class="form-group">
    <label class="control-label col-xs-3" style="color: #0d0f0d" for="name"> <span class="glyphicon glyphicon-user"></span> Имя *:</label>
    <div class="col-xs-9">
      <input type="text" name= "feedback[name]" style="width:200px" class="form-control" id="name" placeholder="Введите имя" required="" oninvalid="this.setCustomValidity('Данное поле должно быть заполнено!')" oninput="setCustomValidity('')">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3"style="color: #0d0f0d" for="inputEmail"> <span class="glyphicon glyphicon-envelope"></span> Email *:</label>
    <div class="col-xs-9">
      <input type="email" name= "feedback[email]" style="width:200px"class="form-control" id="inputEmail" placeholder="Email" required="" oninvalid="this.setCustomValidity('Данное поле должно быть заполнено!')" oninput="setCustomValidity('')">
    </div>
  </div>
  <div class="form-group">
    <label class="control-label col-xs-3" style="color: #0d0f0d" "width:100px" for="inputPassword"> <span class="glyphicon glyphicon-comment"></span> Сообщение *:</label>
    <div class="col-xs-9">
      <textarea type="message" name= "feedback[message]" rows= "7" cols= "70" style="width:350px" class="form-control" id="inputPassword" placeholder="Введите сообщение" required="" oninvalid="this.setCustomValidity('Данное поле должно быть заполнено!')" oninput="setCustomValidity('')"></textarea>
    </div>
  </div>
  <br />
   <div class="form-group">
    <label class="control-label col-xs-3"style="color: #0d0f0d"><?php echo $capt ?></label>
    <div class="col-xs-9">
      <input type="text" name='captcha' style="width:215px"class="form-control" id="captcha" placeholder="Введите символы с картинки">
    </div>
  </div>
  <div class="form-group">
    <div class="col-xs-offset-3 col-xs-9">
      <input type="submit" class="btn btn-primary" value="Отправить">
      <input type="reset" class="btn btn-default" value="Очистить форму">
    </div>
  </div>
</form>