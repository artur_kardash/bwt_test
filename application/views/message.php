 <div id="message">
  <h3 style="color:#0a0505"> <span class="glyphicon glyphicon-info-sign" > </span> Все сообщения:</h3>
   <table class="table table-hover" border="3" width="50%">
   <thead>
	    <tr> 
	        <th width="5%"> 'ID' </th>
	        <th width="20%"> 'Имя' </th>
	        <th width="20%"> 'Email' </th>
	        <th width="200"> 'Сообщение' </th>
	    </tr>
   </thead>
  	<tbody>
  	 <?php foreach($feed as $feedback): ?>		
	    <tr>
	        <td><?php echo $feedback['id']; ?></td>
	        <td><?php echo $feedback['name']; ?></td>
	        <td><?php echo $feedback['email']?></td>
	        <td><?php echo $feedback['message']; ?></td>	 			
	  	</tr> 
	  <?php endforeach; ?>
	</tbody>
  </table>
</div>